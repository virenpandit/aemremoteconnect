mvn archetype:generate -DgroupId=com.offbelay.xpath -DartifactId=XPathAnalyzer -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

To RUN:
USE:
use Maven shade plug-in to include JAR dependencies in final JAR-file
set classpath=.\target\uber-XPathAnalyzer-1.0-SNAPSHOT.jar;%classpath%

mvn clean package
java com.offbelay.aem.AEMClient

OR
To use Maven exec:java
mvn clean compile test


PLUGINS

    Run within maven using exec:exec:
        mvn clean compile test
    pom.xml addition:-
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>exec-maven-plugin</artifactId>
                    <version>1.1.1</version>
                    <executions>
                        <execution>
                            <phase>test</phase>
                            <goals>
                                <goal>java</goal>
                            </goals>
                            <configuration>
                                <mainClass>com.offbelay.aem.AEMClient</mainClass>
                                <!--
                                <arguments>
                                    <argument>arg0</argument>
                                    <argument>arg1</argument>
                                </arguments>
                                -->
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

    For building a Standalone JAR using Maven Shade Plugin
    Run As: 
        maven clean package
        set CLASSPATH=C:\apps\programs\RemoteAEM\target\Uber-AEMClient-1.0-SNAPSHOT.jar;%CLASSPATH%
        java com.offbelay.aem.AEMClient
    pom.xml addition:-
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <executions>
                        <execution>
                            <phase>package</phase>
                            <goals>
                                <goal>shade</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                      <finalName>Uber-${artifactId}-${version}</finalName>
                      <filters>
                        <filter>
                          <artifact>*:*</artifact>
                          <excludes>
                            <exclude>META-INF/*.SF</exclude>
                            <exclude>META-INF/*.DSA</exclude>
                            <exclude>META-INF/*.RSA</exclude>
                          </excludes>
                        </filter>
                      </filters>
                    </configuration>
                </plugin>

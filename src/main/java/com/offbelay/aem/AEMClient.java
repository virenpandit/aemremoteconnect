package com.offbelay.aem;

import org.apache.sling.api.resource.ResourceResolverFactory;
import javax.jcr.Repository; 
import javax.jcr.Session; 
import javax.jcr.SimpleCredentials; 
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import org.apache.jackrabbit.commons.JcrUtils;

import org.apache.commons.lang3.StringUtils;

import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.QueryManager;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class AEMClient {
    
    private static final Logger LOG = Logger.getLogger(AEMClient.class);
        
    String url = "http://localhost:4502/crx/server";
    String username = "admin";
    String password = "admin";
    
    public static void main(String args[]) throws Exception {

        LOG.debug("Inside main.v5()");
        AEMClient client = new AEMClient();
        //client.testQuery();
        //client.testCRUD();
        client.testOps();
    }

    public Session login() throws Exception {
        Session session = null;
        try {
            Repository repository = JcrUtils.getRepository(url);

            LOG.debug("Before login");
            session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()), "crx.default");

            LOG.debug("Connected to repository!");
        } catch(Exception ex) {
            LOG.error("Could not connect to repository" + ex.toString());
            //ex.printStackTrace();
        }
        return session;
    }
    
    public void testQuery() throws Exception {
        try {
            Repository repository = JcrUtils.getRepository(url);

            LOG.debug("Before login");
            javax.jcr.Session session = login();
            LOG.debug("Connected to repository!");

            QueryManager queryManager = session.getWorkspace().getQueryManager();
            Query query = queryManager.createQuery("/jcr:root/content/myportal/*", Query.XPATH);

            QueryResult result = query.execute();
            NodeIterator nodeIter = result.getNodes();

            while (nodeIter.hasNext() ) {
                Node node = nodeIter.nextNode();
                LOG.debug("Found node matching query: " + node.getName());
            }


            session.logout();
            
        } catch(Exception ex) {
            LOG.error("Exception in testQuery(): ", ex);
        }   
    }
    
    class JCRProperty {
        public JCRProperty(String n, Object v) {
            this.name = n;
            this.value = v;
        }
        public String name;
        public Object value;
    }
    private void testOps() throws Exception {
        
        Session session = null;
        try {
            LOG.debug("Inside testOps()");
            session = login();

            // Create recursive nodes
            String[] allNodes = new String[] { "content", "test1", "test2", "test3" };
            String nodeName = "";
            javax.jcr.Node jcrNode = null;
            LOG.debug("Creating JCR Node structure");
            for( String thisNode: allNodes ) {
                // nodeName = nodeName + "/" + thisNode;
                nodeName = thisNode;
                LOG.debug("for-loop: Evaluating nodeName: " + nodeName);
                jcrNode = createJCRNodeIfMissing(nodeName, "cq:Page", jcrNode, session);
            }
            
            // Fill in node with properties
            ArrayList<JCRProperty> list = new ArrayList<JCRProperty>();
            JCRProperty jcrProperty = new JCRProperty("prop1", "value1");
            list.add(jcrProperty);
            jcrProperty = new JCRProperty("prop2", "value2");
            list.add(jcrProperty);
            Object o = new String[]{"test1", "test2", "test3", "test4", "test5"};
            jcrProperty = new JCRProperty("prop3", o);
            list.add(jcrProperty);
            
            if(null!=jcrNode) {
                for( JCRProperty property: list ) {
                    addNodeProperty(jcrNode, property.name, property.value, session);
                    session.save();
                }
            }
            
        } catch(Exception ex) {
            LOG.error("Exception in testOps()", ex);
        } finally {
            try {
                session.logout();
            } catch(Exception iex) { }
        }

        LOG.debug("Exiting testOps()");
    }

    private void addNodeProperty(Node node, String propertyName, Object value, Session session) throws Exception {
        LOG.debug("Inside addNodeProperty(node=" + node + ", type=" + node.getPrimaryNodeType() + ")");
        if(node.getPrimaryNodeType().isNodeType("cq:Page")) {
            LOG.debug("Node is of type: cq:Page: Adding jcr:content if missing");
            node = createJCRNodeIfMissing("jcr:content", "cq:PageContent", node, session);
        }
        if(value instanceof String[]) {
            node.setProperty(propertyName, (String[])value);
        } else {
            LOG.debug("Adding Property: propertyName: " + propertyName + ", value=" + value + " to node: " + node.getName());
            node.setProperty(propertyName, (String)value);
        }
    }

    private Node createJCRNodeIfMissing(String name, String nodeType, Node parentNode, Session session) throws Exception {
        boolean shouldSave = false;
        Node node = null;
        if(null==parentNode) {
            parentNode = session.getRootNode();
        }
        LOG.debug("Inside createJCRNodeIfMissing(node=" + name + ")");
        
        LOG.debug("Checking first if node exists: " + name + " under Parent: [" + parentNode.getPath() + "]");
        if(parentNode.hasNode(name)) {
            LOG.debug("createJCRNodeIfMissing(): Node: [" +  name + "] already exists under Parent: [" + parentNode.getPath() + "] No need to recreate");
            node = parentNode.getNode(name);
        } else {
            LOG.debug("This node does not exist [" + name + "] under Parent: [" + parentNode.getPath() + "]");
            if(null!=parentNode) {
                LOG.debug("createJCRNodeIfMissing(): Creating node: [" + name + "] under parent: [" + parentNode.getName() + "]");
                node = parentNode.addNode(name, nodeType);
                if(StringUtils.equalsIgnoreCase(nodeType, "cq:Page")) {
                    LOG.debug("Adding PageContent (jcr:content) sub-node to node: " + node.getName());
                    createJCRNodeIfMissing("jcr:content", "cq:PageContent", node, session);
                }
                shouldSave = true;
            } else {
                LOG.debug("createJCRNodeIfMissing(): Creating node: [" + name + "] at /");
                node = session.getRootNode().addNode(name, nodeType);
                shouldSave = true;
            }
        }
        if(shouldSave) {
            LOG.debug("Commiting changes to JCR Session");
            session.save();
        }
        if(null!=node)
            LOG.debug("Exiting createJCRNodeIfMissing(node=" + node.getName() + ")");
        else
            LOG.debug("Exiting createJCRNodeIfMissing(node=" + node + ")");
        return node;
    }
    
    
       
    
    
    public void testCRUD() throws Exception {
        try {
            Repository repository = JcrUtils.getRepository(url);

            LOG.debug("Before login");
            javax.jcr.Session session = login();
            LOG.debug("Connected to repository!");

            Node root = session.getRootNode(); //Or getNode("/content");

            // Store content 
            Node day = root.addNode("adobe");
            day.setProperty("message", "Adobe Experience Manager is part of the Adobe Digital Product Suite");
            session.save();

            // Retrieve content 
            Node node = root.getNode("adobe"); 
            LOG.debug(node.getPath()); 
            LOG.debug(node.getProperty("message").getString());

            // Retrieve Attribute Array
            // // Retrieve content 
            // Node node = root.getNode("adobe/cq"); 
            // Property references = node.getProperty("testprop");  
            // Value[] values = references.getValues();
            // String myVal = values[0].getString();

            session.logout();
            
        } catch(Exception ex) {
            LOG.error("Exception in testCRUD(): " + ex.toString());
            //ex.printStackTrace();
        }
    }
}
